const User = require("../models/User");
const Room = require("../models/Room");
const Transaction = require("../models/Transaction");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.userCheckIn = (data) => {
  if (data.isAdmin == true) {
    return false;
  } else {
    return User.findById(data.userId).then((user) => {
      return Room.find({ number: data.roomNumber }).then((result) => {
        let newTransaction = new Transaction({
          userId: data.userId,
          roomId: result[0]._id,
          roomNumber: data.roomNumber,
          duration: data.duration,
          totalPrice: data.duration * result[0].price,
        });

        return newTransaction.save().then((transaction, error) => {
          if (error) {
            return false;
          } else {
            return true;
          }
        });
      });
    });
  }
};

// Retrieve All Orders Admin
module.exports.All = (data) => {
  const AllOrders = [];

  return Transaction.find({}).then((result) => {
    for (let i = 0; i < result.length; i++) {
      AllOrders.push(result[i]);
    }
    return AllOrders;
  });
};

// Retrieve User Order
module.exports.getUserCheckin = (userData) => {
  return Transaction.find({ userId: userData.id }).then((result) => {
    if (result == null) {
      return false;
    } else {
      return result;
    }
  });
};
