const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Room = require("../models/Room");

// REGISTER USER
module.exports.registerUser = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return "Email has already been taken.";
    } else {
      let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,

        password: bcrypt.hashSync(reqBody.password, 10),
      });

      return newUser.save().then((user, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
    }
  });
};

// LOGIN USER
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return { message: "Incorrect Email / Password" };
      }
    }
  });
};

// Make User Admin
module.exports.makeAdmin = (data) => {
  return User.findById(data.userId).then((result, err) => {
    result.isAdmin = true;

    return result.save().then((adminUser, err) => {
      if (err) {
        return false;
      } else {
        return true;
      }
    });
  });
};

// Retrieve User Details
module.exports.getProfile = (userData) => {
  return User.findById(userData.id).then((result) => {
    if (result == null) {
      return false;
    } else {
      result.password = "*****";

      return result;
    }
  });
};
