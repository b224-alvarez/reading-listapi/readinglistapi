const Room = require("../models/Room");
const User = require("../models/User");

// Create Room (ADMIN ONLY)
module.exports.createRoom = (reqBody) => {
  let newRoom = new Room({
    number: reqBody.number,
    type: reqBody.type,
    description: reqBody.description,
    price: reqBody.price,
  });

  return newRoom.save().then((room, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

// Retrieve All Products (ADMIN ONLY)
module.exports.getAllRooms = (data) => {
  return Room.find({}).then((result) => {
    return result;
  });
};

// Retrieve All Active Rooms
module.exports.getAllActive = () => {
  return Room.find({ isActive: true }).then((result) => {
    return result;
  });
};

// Retrieve a Single Room
module.exports.getRoom = (reqParams) => {
  return Room.findById(reqParams.roomId).then((result) => {
    return result;
  });
};

// Update a Product (ADMIN ONLY)
module.exports.updateRoom = (reqParams, reqBody) => {
  let updatedRoom = {
    number: reqBody.number,
    type: reqBody.type,
    description: reqBody.description,
    price: reqBody.price,
  };

  return Room.findByIdAndUpdate(reqParams.roomId, updatedRoom).then(
    (updatedRoom, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// Archive a Product (ADMIN ONLY)
module.exports.archiveRoom = (data) => {
  return Room.findById(data.roomId).then((result, err) => {
    if (result.isActive == true) {
      result.isActive = false;

      return result.save().then((archivedRoom, err) => {
        if (err) {
          return false;
        } else {
          return true;
        }
      });
    } else {
      return false;
    }
  });
};

// Activate a Product (ADMIN ONLY)
module.exports.activateRoom = (data) => {
  return Room.findById(data.roomId).then((result, err) => {
    if (result.isActive == false) {
      result.isActive = true;

      return result.save().then((archivedRoom, err) => {
        if (err) {
          return false;
        } else {
          return true;
        }
      });
    } else {
      return false;
    }
  });
};
