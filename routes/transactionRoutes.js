const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const transactionController = require("../controllers/transactionController");
const auth = require("../auth");

// User Order Route
router.post("/checkin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let data = {
    userId: userData.id,
    isAdmin: userData.isAdmin,
    roomNumber: req.body.roomNumber,
    duration: req.body.duration,
  };

  transactionController
    .userCheckIn(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve All Checkins Route
router.get("/All", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    transactionController
      .All(userData)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "This function is not available for users." });
  }
});

// Retrieve User Orders Route
router.get("/mycheckin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  transactionController
    .getUserCheckin({ id: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
