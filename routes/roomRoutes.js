const express = require("express");
const router = express.Router();
const roomController = require("../controllers/roomController");
const auth = require("../auth");

// Create Product Route (ADMIN ONLY)
router.post("/createRoom", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    roomController
      .createRoom(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "This function is not available for users." });
  }
});

// Retrieve All Products Route (ADMIN ONLY)
router.get("/all", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    roomController
      .getAllRooms(userData)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "This function is not available for users." });
  }
});

// Retrieve All Active Products Route
router.get("/", (req, res) => {
  roomController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve Single Product Route
router.get("/:roomId", (req, res) => {
  roomController
    .getRoom(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Update a Product Route (ADMIN ONLY)
router.put("/update/:roomId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    roomController
      .updateRoom(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "This function is not available for users." });
  }
});

// Archive a Product (ADMIN ONLY)
router.put("/archive/:roomId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    roomController
      .archiveRoom(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "This function is not available for users." });
  }
});

// Activate a Product (ADMIN ONLY)
router.put("/activate/:roomId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    roomController
      .activateRoom(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "This function is not available for users." });
  }
});

module.exports = router;
