const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// USER REGISTRATION
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// USER LOGIN
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Make User Admin Route
router.put("/admin/:userId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    userController
      .makeAdmin(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "This function is not available for users." });
  }
});

// Retrieve User Detail Route
router.post("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  userController
    .getProfile({ id: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
