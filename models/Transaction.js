const mongoose = require("mongoose");

const transactionSchema = new mongoose.Schema({
  userId: {
    type: String,
  },
  roomId: {
    type: String,
  },
  roomNumber: {
    type: Number,
  },
  duration: {
    type: Number,
  },
  totalPrice: {
    type: Number,
  },
  bookedOn: {
    type: String,
    default: new Date(),
  },
});

module.exports = mongoose.model("Transaction", transactionSchema);
