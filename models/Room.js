const mongoose = require("mongoose");

const roomSchema = new mongoose.Schema({
  number: {
    type: Number,
    required: [true, "Room Number is required"],
  },
  type: {
    type: String,
    required: [true, "Room Type is required"],
  },
  description: {
    type: String,
    required: [true, "Room Description is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  price: {
    type: Number,
    required: [true, "Room price is required"],
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Room", roomSchema);
